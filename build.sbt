name := "zgames"

version := "1.0-SNAPSHOT"

resolvers += "Mandubian repository snapshots" at "https://github.com/mandubian/mandubian-mvn/raw/master/snapshots/"

resolvers += "Mandubian repository releases" at "https://github.com/mandubian/mandubian-mvn/raw/master/releases/"

libraryDependencies ++= Seq(
  "org.mandubian" %% "play-actor-room" % "0.1",
  cache
)     

play.Project.playScalaSettings
