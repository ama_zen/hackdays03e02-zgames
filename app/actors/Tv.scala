/*
 * This file is part of BYOG.
 *
 * Copyright 2014 Zengularity
 *
 * BYOG is free software: you can redistribute it and/or modify
 * it under the terms of the AFFERO GNU General Public License as published by
 * the Free Software Foundation.
 *
 * BYOG is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
 * the AFFERO GNU General Public License for the complete license terms.
 *
 * You should have received a copy of the AFFERO GNU General Public License
 * along with BYOG.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
 */
package actors

import scala.concurrent._
import scala.concurrent.duration._

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import play.api._
import play.api.mvc._
import play.api.libs.iteratee._
import play.api.libs.json._
import play.api.libs.concurrent._
import play.api.mvc.WebSocket.FrameFormatter
import java.util.concurrent.atomic.AtomicLong

// Implicits
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

import org.mandubian.actorroom._

class Tv(val id: String) extends Actor {

  implicit val playerSTtausFormatter = Json.format[PlayerStatus]
  implicit val warmUpFormat = Json.format[WarmUp]
  implicit val gamestateFormatter = Json.format[GameState]
  implicit val scoreFormatter = Json.format[Score]
  implicit val scoresFormatter = Json.format[Scores]

  def receive = {

    case Received(from, js: JsValue) => {
      (js \ "msg").asOpt[String] match {
        case None => play.Logger.error("couldn't msg in websocket event")
        case Some(s) => {
          play.Logger.info(s"received $s from $from")
          context.parent ! s
        }
      }
    }
    case gs:GameState => {
      context.parent ! Send(id, id, Json.obj("screen" -> 1, "data" -> Json.toJson(gs)))
    }
    case s:Scores => {
      context.parent ! Send(id, id, Json.obj("screen" -> 2, "data" -> s.scores))
    }
    case w:WarmUp => {
      context.parent ! Send(id, id, Json.obj("screen" -> 1, "data" -> Json.toJson(w)))
    }
  }
}
