/*
 * This file is part of BYOG.
 *
 * Copyright 2014 Zengularity
 *
 * BYOG is free software: you can redistribute it and/or modify
 * it under the terms of the AFFERO GNU General Public License as published by
 * the Free Software Foundation.
 *
 * BYOG is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
 * the AFFERO GNU General Public License for the complete license terms.
 *
 * You should have received a copy of the AFFERO GNU General Public License
 * along with BYOG.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
 */
package actors

import scala.concurrent._
import scala.concurrent.duration._

import play.api._
import play.api.mvc._
import play.api.libs.iteratee._
import play.api.libs.json._
import play.api.libs.concurrent._
import akka.actor.ActorRef

// Implicits
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

import org.mandubian.actorroom._

case class PlayerStatus(alive: Boolean, position: Float, color:String)

case class GameState(positions: Map[String, PlayerStatus], ballPosition: Float)
case class WarmUp(positions: Map[String, PlayerStatus], ballPosition: Float)
case class Tap(pid : String)
case class Ready(pid : String)
case class Stop(rank: Int, distance: Float)
case class Scores(scores: List[Score])

case class Score(id: String, position: Float)

case object Restart
case object Tick
case object Start // Start from gamepad
case object Go    // internal go from server (after the warmup)


class Game extends Supervisor {

  type Payload = JsValue
  val positions : scala.collection.mutable.Map[String, PlayerStatus] = scala.collection.mutable.Map.empty
  var started:Boolean = false
  var finished:Boolean = false
  var ballPosition: Float = 0f
  var owner: Option[String] = None

  val colors = Seq(
    "#435C81",
    "#DC7A36",
    "#E0644F",
    "#27A990",
    "#4A7F9E",
    "#71669F",
    "#A6ABB1",
    "#71AED9",
    "#8ED5ED",
    "#51CC96",
    "#E8936D",
    "#9A91EA",
    "#122E4B"
  )
  var ballStep = 0.2f
  val ballSpeed = 0.01f
  val playerStep = 0.4f

  def initBallStep() { ballStep = 0.2f }

  def getPlayer(id: String) : Option[PlayerStatus] = positions.get(id)

  def gameReceive: Receive = {

    case ready: Ready => {
      if(!started) {
        val isOwner = if(owner == None) { owner = Some(ready.pid) ; true }
                      else false
        val color = colors(positions.size % colors.size)
        positions(ready.pid) = PlayerStatus(true, 7, color)
        sendJson(ready.pid, Json.obj("action" -> "waiting", "owner" -> isOwner, "color" -> color))
      } else {
        sendJson(ready.pid, Json.obj("action" -> "error", "data" -> "Game already started"))
      }
    }

    case disconnect: Disconnected => {
      super.receive(disconnect)
      positions -= disconnect.id
      if(owner == Some(disconnect.id)) owner = None
    }

    case Restart => {
      println("restart")
      started = false
      finished = false
      positions.clear
      ballPosition = 0f
    }
    case Go => {
      started = true
    }
    case Start => {
      initBallStep()
      context.system.scheduler.scheduleOnce(3000 milliseconds, self, Go)
      members.foreach {
        case (id, member) => member.receiver ! WarmUp(positions.toMap, ballPosition)
      }
    }

    case t:Tap => {
      if(started && getPlayer(t.pid).map(_.alive).getOrElse(false))
        positions(t.pid) = positions(t.pid).copy(position = positions(t.pid).position + playerStep)
    }

    case Tick => {
      if(started && !finished) {
        ballStep = ballStep + ballSpeed
        ballPosition += ballStep
        if(positions.filter(_._2.alive).isEmpty) {
          ballPosition = 0f
          finished = true
          owner = None
          positions.clear
          started = false
        } else {
          members.foreach {
            case (id, member) => {
              positions.get(id) match {
                case Some(ps:PlayerStatus) => {
                  if (ps.alive && ps.position <= ballPosition) {
                    println(s" $id is dead!")
                    positions(id) = ps.copy(alive = false)
                    member.receiver ! Stop(getPlayerRank(id), positions(id).position)
                  }
                }
                case None => { }
              }
              member.receiver ! GameState(positions.toMap, ballPosition)
            }
          }
        }
      }
    }
  }

  def getPlayerRank(id: String) = {
    positions.toList.sortBy(t => t._2.position).reverse.indexOf((id, positions(id)))+1
  }

  override def receive = gameReceive orElse super.receive

  def sendJson(pid: String, json: JsValue) = {
    members(pid).sender ! Send(pid, "-1", json)
  }

}
