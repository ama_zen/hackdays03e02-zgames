/*
 * This file is part of BYOG.
 *
 * Copyright 2014 Zengularity
 *
 * BYOG is free software: you can redistribute it and/or modify
 * it under the terms of the AFFERO GNU General Public License as published by
 * the Free Software Foundation.
 *
 * BYOG is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
 * the AFFERO GNU General Public License for the complete license terms.
 *
 * You should have received a copy of the AFFERO GNU General Public License
 * along with BYOG.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
 */
package actors

import scala.concurrent._
import scala.concurrent.duration._

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import play.api._
import play.api.mvc._
import play.api.libs.iteratee._
import play.api.libs.json._
import play.api.libs.concurrent._
import play.api.mvc.WebSocket.FrameFormatter
import java.util.concurrent.atomic.AtomicLong

// Implicits
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

import org.mandubian.actorroom._

class Player(val id: String) extends Actor {

  type Payload = JsValue

  def receive = {
    case Received(from, js: JsValue) => {
      play.Logger.info(s"received $js from $from")
      (js \ "msg").asOpt[String] match {
        case None => play.Logger.error("couldn't msg in websocket event")
        case Some("tap") => {
          context.parent ! Tap(from)
        }
        case Some("start") => {
          context.parent ! Start
        }
        case Some("restart") => {
          context.parent ! Restart
        }
        case Some("ready") => {
          context.parent ! Ready(from)
        }
      }
    }
    case w:WarmUp => {
      context.parent ! Send(id, id, Json.obj("action" -> "warmup"))
    }
    case s:Stop => {
      context.parent ! Send(id, id, Json.obj("action" -> "stop", "rank" -> s.rank, "distance" -> s.distance))
    }
  }
}
