/*
 * This file is part of BYOG.
 *
 * Copyright 2014 Zengularity
 *
 * BYOG is free software: you can redistribute it and/or modify
 * it under the terms of the AFFERO GNU General Public License as published by
 * the Free Software Foundation.
 *
 * BYOG is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
 * the AFFERO GNU General Public License for the complete license terms.
 *
 * You should have received a copy of the AFFERO GNU General Public License
 * along with BYOG.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
 */
package controllers

import scala.concurrent._
import scala.concurrent.duration._

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import play.api._
import play.api.mvc._
import play.api.libs.iteratee._
import play.api.libs.json._
import play.api.libs.concurrent._
import play.api.mvc.WebSocket.FrameFormatter
import java.util.concurrent.atomic.AtomicLong

// Implicits
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

import org.mandubian.actorroom._
import actors._

object Application extends Controller {

  val room = Room(Props(classOf[Game]))
  val cancellable =
    Akka.system.scheduler.schedule(
      0 milliseconds,
      100 milliseconds,
      room.supervisor,
      Tick)

  def index = Action { implicit r =>
    Ok(views.html.index("Your new application is ready."))
  }

  def websocketTv(id: String) = {
    room.websocket[JsValue]((_: RequestHeader) => id, Props(classOf[Tv], id))
  }

  def websocketPad(id: String) = {
    room.websocket[JsValue]((_: RequestHeader) => id, Props(classOf[Player], id))
  }

  def gamepad = Action { implicit r =>
    Ok(views.html.gamepad())
  }

  def tv = Action { implicit r =>
    Ok(views.html.tv())
  }
}
