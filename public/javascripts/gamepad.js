/*!
 * This file is part of BYOG.
 *
 * Copyright 2014 Zengularity
 *
 * BYOG is free software: you can redistribute it and/or modify
 * it under the terms of the AFFERO GNU General Public License as published by
 * the Free Software Foundation.
 *
 * BYOG is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
 * the AFFERO GNU General Public License for the complete license terms.
 *
 * You should have received a copy of the AFFERO GNU General Public License
 * along with BYOG.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
 */
/*jshint browser:true */
(function () {
  "use strict";

  var gamepadDiv;
  var socket;
  var color;

  var connectionDiv;
  var connectionMessage;

  var errorDiv;
  var errorMessage;
  var errorButton;

  var waitingDiv;
  var waitingOwnerStartButton;
  var waitingClientMessage;

  var warmupDiv;
  var warmupCountdown;

  var gameDiv;
  var gameRunButton;
  var touch = false;

  var scoreDiv;
  var scorePosition;
  var scoreDistance;
  var scoreRestartButton;

  // ****************************************************
  // ****************************************************
  // ***
  // ***    INIT
  // ***
  // ****************************************************
  // ****************************************************

  function init() {
    initAll();
    start();
  }

  function start() {
    startConnection();
  }

  function startConnection() {
    connectionStart();
    try {
      if (socket) socket.close();
    } catch (e) { /* OSEF */ }
    if (window.MozWebSocket) {
      window.WebSocket = window.MozWebSocket;
    }
    var id = Math.round(Math.random() * 999999999);
    var url = CONFIG.websocketUrl.replace(/--port--/, id);
    socket = new WebSocket(url);
    socket.onmessage = function (e) {
      var json = JSON.parse(e.data);
      debug("Received:", json);
      switch (json.action) {
        case "waiting": waitingStart(json); break;
        case "warmup": warmupStart(json); break;
        case "stop": scoreStart(json); break;
        case "error": errorStart(json); break;
        default: console.log("Bad message:", json);
      }
    };
    socket.onopen = function () {
      sendMessage({msg: "ready"});
    };
  }

  function sendMessage(msg) {
    debug("Send:", msg);
    socket.send(JSON.stringify(msg));
  }

  function initAll() {
    gamepadDiv = document.querySelector("#gamepad");
    connectionInit();
    errorInit();
    waitingInit();
    warmupInit();
    gameInit();
    scoreInit();
  }

  function stopAll() {
    connectionStop();
    errorStop();
    waitingStop();
    warmupStop();
    gameStop();
    scoreStop();
  }

  // ****************************************************
  // ****************************************************
  // ***
  // ***    CONNECTION
  // ***
  // ****************************************************
  // ****************************************************

  function connectionInit() {
    connectionDiv = document.querySelector("#connection");
    connectionMessage = connectionDiv.querySelector("#connection-msg");
  }

  function connectionStart() {
    stopAll();
    connectionDiv.style.display = "block";
  }

  function connectionStop() {
    connectionDiv.style.display = "none";
  }

  // ****************************************************
  // ****************************************************
  // ***
  // ***    ERROR
  // ***
  // ****************************************************
  // ****************************************************

  function errorInit() {
    errorDiv = document.querySelector("#error");
    errorMessage = errorDiv.querySelector("#error-msg div");
    errorButton = errorDiv.querySelector("#error-btn");
    errorButton.addEventListener("click", errorDoRetry);
  }

  // {action: "error", data: "Game already started"}
  function errorStart(json) {
    stopAll();
    errorDiv.style.display = "block";
    errorMessage.innerHTML = json.data;
  }

  function errorStop() {
    errorDiv.style.display = "none";
  }

  function errorDoRetry() {
    start();
  }

  // ****************************************************
  // ****************************************************
  // ***
  // ***    WAITING
  // ***
  // ****************************************************
  // ****************************************************

  function waitingInit() {
    waitingDiv = document.querySelector("#waiting");
    waitingOwnerStartButton = waitingDiv.querySelector("#waiting-btn");
    waitingClientMessage = waitingDiv.querySelector("#waiting-msg");
    waitingOwnerStartButton.addEventListener("click", waitingDoStart);
  }

  // {action: waiting, owner: true, color: "red"}
  function waitingStart(json) {
    stopAll();
    color = json.color;
    waitingOwnerStartButton.style.backgroundColor = color;
    waitingClientMessage.style.backgroundColor = color;
    waitingDiv.style.display = "block";
    if (json.owner) {
      waitingOwnerStartButton.style.display = "table";
      waitingClientMessage.style.display = "none";
    } else {
      waitingOwnerStartButton.style.display = "none";
      waitingClientMessage.style.display = "table";
    }
  }

  function waitingStop() {
    waitingDiv.style.display = "none";
  }

  function waitingDoStart() {
    sendMessage({msg: "start"});
  }

  // ****************************************************
  // ****************************************************
  // ***
  // ***    WARMUP
  // ***
  // ****************************************************
  // ****************************************************

  function warmupInit() {
    warmupDiv = document.querySelector("#warmup");
    warmupCountdown = warmupDiv.querySelector("#warmup-countdown div");
  }

  // {action: "warmup"}
  function warmupStart(json) {
    stopAll();
    warmupCountdown.style.backgroundColor = color;
    warmupDiv.style.display = "block";
    warmupCountdown.innerHTML = "3";
    function warm(sec) {
      if (sec > 0) {
        warmupCountdown.innerHTML = sec;
        setTimeout(function () {
          warm(sec - 1);
        }, 700);
      } else {
        gameStart();
      }
    }
    warm(3);
  }

  function warmupStop() {
    warmupDiv.style.display = "none";
  }

  // ****************************************************
  // ****************************************************
  // ***
  // ***    GAME
  // ***
  // ****************************************************
  // ****************************************************

  function gameInit() {
    gameDiv = document.querySelector("#game");
    gameRunButton = gameDiv.querySelector("#game-btn");
  }

  function gameStart() {
    stopAll();
    gameRunButton.style.backgroundColor = color;
    gameDiv.style.display = "block";
    gameRunButton.addEventListener("click", gameOnClick, false);
    gameRunButton.addEventListener("touchstart", gameOnTouchstart, false);
    gameRunButton.addEventListener("touchend", gameOnTouchend, false);
  }

  function gameStop() {
    gameDiv.style.display = "none";
    gameRunButton.removeEventListener("click");
    gameRunButton.removeEventListener("touchstart");
    gameRunButton.removeEventListener("touchend");
  }

  function gameOnClick() {
    if (!touch) gameDoRun();
  }

  function gameOnTouchstart(e) {
    e.preventDefault();
    touch = true;
  }
  function gameOnTouchend() {
    gameDoRun();
  }

  function gameDoRun() {
    if (gameRunButton.style.backgroundColor == color) {
      gameRunButton.style.backgroundColor = "gray";
    } else {
      gameRunButton.style.backgroundColor = color;
    }
    sendMessage({msg: "tap"});
  }

  // ****************************************************
  // ****************************************************
  // ***
  // ***    SCORE
  // ***
  // ****************************************************
  // ****************************************************

  function scoreInit() {
    scoreDiv = document.querySelector("#score");
    scorePosition = scoreDiv.querySelector("#score-position div");
    scoreDistance = scoreDiv.querySelector("#score-distance div");
    scoreRestartButton = scoreDiv.querySelector("#score-btn");
    scoreRestartButton.addEventListener("click", scoreDoRestart);
  }

  // {action: "stop", rank: 2, distance: 2013}
  function scoreStart(json) {
    stopAll();
    scorePosition.style.backgroundColor = color;
    scoreDistance.style.backgroundColor = color;
    scoreDiv.style.display = "block";
    scoreDisplayPosition(json.rank);
    scoreDisplayDistance(json.distance);
  }

  function scoreStop() {
    scoreDiv.style.display = "none";
  }

  function scoreDisplayPosition(rank) {
    var str;
    switch (rank) {
      case 1: str = "First!!!"; break;
      case 2: str = "Second!"; break;
      case 3: str = "Third!"; break;
      default: str = rank + "th";
    }
    scorePosition.innerHTML = str;
  }

  function scoreDisplayDistance(distance) {
    scoreDistance.innerHTML = Math.round(distance) + "m";
  }

  function scoreDoRestart() {
    start();
  }

  document.Gamepad = {
    init: init
  };

  // ---

  function debug() {
    var args = Array.prototype.slice.apply(arguments);
    args.unshift("DEBUG");
    console.log.apply(console, args);
  }

}());
