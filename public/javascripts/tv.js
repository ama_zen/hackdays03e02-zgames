/*!
 * This file is part of BYOG.
 *
 * Copyright 2014 Zengularity
 *
 * BYOG is free software: you can redistribute it and/or modify
 * it under the terms of the AFFERO GNU General Public License as published by
 * the Free Software Foundation.
 *
 * BYOG is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
 * the AFFERO GNU General Public License for the complete license terms.
 *
 * You should have received a copy of the AFFERO GNU General Public License
 * along with BYOG.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
 */
$(function() {
  var screen = window.document.body.getBoundingClientRect();
  var screenScale = [
    screen.width / 1920,
    screen.height / 1080
  ];
  var gameState = {
    screen : null
  };
  var wsLastMessage = null;
  var game = new Phaser.Game(screen.width, screen.height, Phaser.AUTO);

  game.state.add("waiting", createWaiting(), true);
  game.state.add("game", createGame());
  game.state.add("score", createScore());

  if(window.MozWebSocket) {
    window.WebSocket = window.MozWebSocket;
  }
  window.socket = new WebSocket(CONFIG.websocketUrl)

  socket.onmessage = function(e){
    console.log('ws ' + e.data);
    // Ignore unwanted messages
    if(/kind/.test(e.data)) return;
    wsLastMessage = $.parseJSON(e.data);
    if( wsLastMessage && (gameState.screen !== wsLastMessage.screen)){
      //Changement de d'ecran en reponse au donnees du serveur
      if(wsLastMessage.screen === 0){
        game.state.start("waiting");
      }
      else if(wsLastMessage.screen === 1 ) {
        game.state.start("game");
      }
      else if(wsLastMessage.screen === 2){
        game.state.start("score");
      }
      else {
        console.log("WARN : unknown screen state");
      }
      gameState.screen = wsLastMessage.screen;
    }
  };

  function createWaiting(){
    var titleBG;
    return {
      preload : function(){
        var titleURL = CONFIG.assets.img.title;
        this.load.image("title", titleURL);
      },
      create: function(){
        titleBG = this.add.sprite(game.world.centerX, game.world.centerY, "title");
        titleBG.anchor.setTo(0.5, 0.5);
        titleBG.scale.setTo.apply(titleBG.scale, screenScale);
      }
    };
  };

  function createGame(){
    var stone;
    var bg;
    var players = {};
    var playersSprites;
    var stonePos;
    var createPlayer = function createPlayer(color){
      var playerSprite = game.add.sprite(game.world.centerX, game.world.centerY, color);
      playerSprite.scale.setTo(0.4, 0.4);
      playerSprite.anchor.setTo(0.5, 0.5);
      playerSprite.y = game.height - 50;
      return playerSprite;
    }
    return {
      preload:function(){
        _.forOwn(CONFIG.assets.img, function loadImages(path, key){
          this.load.image(key, path);
        }, game);
      },
      create:function(){
        playersSprites = {};
        bg = this.add.sprite(game.world.centerX, game.world.centerY, 'game');
        stone = this.add.sprite(game.world.centerX, game.world.centerY, 'stone');
        bg.anchor.setTo(0.5, 0.5);
        bg.scale.setTo.apply(bg.scale, screenScale);
        stone.anchor.setTo(0.5, 0.5);
        stone.scale.setTo(0.8, 0.8);
        stone.x = 50;
        stone.y = game.height * 0.65;//Ca marche meme pas ce truc de toutes facons
      },
      update:function(){
        players = wsLastMessage.data.positions;
        stonePos = wsLastMessage.data.ballPosition;
        stone.angle +=10;
        var diff = Math.floor((Math.random()*3)+1);
        stone.y= game.height - (game.height /2.5) + diff;
        for(var key in players) {
          if(!playersSprites[key]){
            playersSprites[key] = createPlayer(players[key].color)
          }
          if(players[key].alive){
            playersSprites[key].x = 350 + (Math.abs(players[key].position - stonePos) * 40)
          } else {
            playersSprites[key].visible=false;
          }
        };
      }
    };
  }

  function createScore(){
    return {};
  }

});
